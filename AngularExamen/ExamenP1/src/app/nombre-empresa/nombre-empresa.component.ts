import { Component } from '@angular/core';

@Component({
  selector: 'app-nombre-empresa',
  standalone: true,
  imports: [],
  templateUrl: './nombre-empresa.component.html',
  styleUrl: './nombre-empresa.component.css'
})
export class NombreEmpresaComponent {
  NombreEmpresa = 'Portal Integral de Deportes y Salud';
}
